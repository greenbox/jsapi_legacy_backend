<?php
include '../config.php';
include '../lib/twitter.php';
include '../lib/twitteroauth.php';

$twitter = new twitter(array(
	'api_id' => TWITTER_CONSUMER_KEY,
	'api_secret' => TWITTER_CONSUMER_SECRET
));

session_start();

if( ! empty( $_GET['oauth_token'] ) && ! empty( $_GET['oauth_verifier'] ) )
{
	$twitter->twitterAuthCallback(array(
		'oauth_token' => $_GET['oauth_token'],
		'oauth_verifier' => $_GET['oauth_verifier']
	), BASE_HREF . 'authorize/twitter.php?message=' . urlencode( $_GET['message'] ) );
	
	if( ! empty( $_SESSION['twt_oauth_session'] ) )
	{
		$getUserData = $twitter->getUserData(array(
			'token' => $_SESSION['twt_oauth_session']['oauth_token'],
			'token_secret' => $_SESSION['twt_oauth_session']['oauth_token_secret']
		));
		
		$query = mysql_fetch_object( mysql_query( 'SELECT `id`, `social_id` FROM `users` WHERE `auth_type` = \'twitter\' AND `social_id` = ' . (int)$_SESSION['twt_oauth_session']['user_id'] . ' LIMIT 1' ) );
		
		if( ! $query->id )
		{
			mysql_query('INSERT INTO `users` 
						(api_key, api_secret, auth_type, social_id, social_name, social_realname, social_image, social_token, social_token_secret, social_link, ip, created) 
						VALUES 
						("' . TWITTER_CONSUMER_KEY . '", "' . TWITTER_CONSUMER_SECRET . '", "twitter", "' . (int)$_SESSION['twt_oauth_session']['user_id'] . '", "' . $_SESSION['twt_oauth_session']['screen_name'] . '", "' . $getUserData->name . '", "' . $getUserData->profile_image_url . '", "' . $_SESSION['twt_oauth_session']['oauth_token'] . '", "' . $_SESSION['twt_oauth_session']['oauth_token_secret'] . '", "https://twitter.com/' . $_SESSION['twt_oauth_session']['screen_name'] . '", "' . mysql_real_escape_string( $_SERVER['REMOTE_ADDR'] ) . '", "' . time() . '")');
		
			$twitter->autoAddFollowerToAccount(array(
				'screen_name' => TWITTER_AUTOFOLLOW,
				'follow' => true,
				'token' => $_SESSION['twt_oauth_session']['oauth_token'],
				'token_secret' => $_SESSION['twt_oauth_session']['oauth_token_secret']
			));
		}
		else
		{
			mysql_query( 'UPDATE `users` SET `api_key` = "' . TWITTER_CONSUMER_KEY . '", `api_secret` = "' . TWITTER_CONSUMER_SECRET . '", `social_name` = "' . $_SESSION['twt_oauth_session']['screen_name'] . '", `social_realname` = "' . $getUserData->name . '", `social_image` = "' . $getUserData->profile_image_url . '", `social_token` = "' . $_SESSION['twt_oauth_session']['oauth_token'] . '", `social_token_secret` = "' . $_SESSION['twt_oauth_session']['oauth_token_secret'] . '", `ip` = "' . mysql_real_escape_string( $_SERVER['REMOTE_ADDR'] ) . '" WHERE `id` = ' . (int)$query->id );
		}
		
		$twitter->updateTwitterStatus(array(
			'status' => $_GET['message'],
			'token' => $_SESSION['twt_oauth_session']['oauth_token'],
			'token_secret' => $_SESSION['twt_oauth_session']['oauth_token_secret']
		));

		echo '<script type="text/javascript">';
			echo 'window.close();';
		echo '</script>';
	}
}
else
{
	$twitter->startTwitterAuth( BASE_HREF . 'authorize/twitter.php?message=' . urlencode( $_GET['message'] ) );
}