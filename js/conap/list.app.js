var CNSApp = Toolbox.Base.extend({ 
    constructor: function (name) {
        this.model = name;
        hostMethod = this;
    },

    commandReturn: function (method) {
    	if (method == 'list'){
            if (typeof yepnope != 'undefined'){
                yepnope({
                  test: typeof $ != 'undefined',
                  nope: 'json2.js',
                  complete: function () {
                    hostMethod.model.addHistory({
                        command : method,
                        result : "Delayed answer ..."
                    });
                  }
                });
                console.log('Return Delay');
                return 'delay';
            }else{
                return 'Called From Wrong place';
            }
        }else{
        	return false;
        }
    } 
});