<?php
class twitter
{
	protected $api_id;
	protected $api_secret;
	
	function __construct( $config = array() )
	{
		$this->setApiId( $config['api_id'] );
		$this->setApiSecret( $config['api_secret'] );
	}
	
	function setApiId( $api_id )
	{
		$this->api_id = $api_id;
		
		return $this;
	}
	
	function setApiSecret( $api_secret )
	{
		$this->api_secret = $api_secret;
		
		return $this;
	}
	
	function getApiId()
	{
		return $this->api_id;
	}
	
	function getApiSecret()
	{
		return $this->api_secret;
	}
	
	function startTwitterAuth( $authorize_url )
	{
		$connection = new TwitterOAuth( $this->getApiId(), $this->getApiSecret() );
		
		$token = $connection->getRequestToken( $authorize_url );
		
		$_SESSION['twt_oauth_token'] = $token['oauth_token'];
		$_SESSION['twt_oauth_token_secret'] = $token['oauth_token_secret'];
		
		header( 'Location: ' . $connection->getAuthorizeURL( $token['oauth_token'] ) );
	}
	
	function twitterAuthCallback( $criteria = array(), $authorize_url )
	{
		$oauth_token = $criteria['oauth_token'];
		$oauth_verifier = $criteria['oauth_verifier'];
		
		if( isset( $oauth_token ) && $_SESSION['twt_oauth_token'] != $oauth_token )
		{
			header( 'Location: ' . $authorize_url );
		}
		
		$connection = new TwitterOAuth( $this->getApiId(), $this->getApiSecret(), $oauth_token, $_SESSION['twt_oauth_token_secret'] );
		
		$access_token = $connection->getAccessToken( $oauth_verifier );
		
		$_SESSION['twt_oauth_session'] = $access_token;
		
		unset( $_SESSION['twt_oauth_token'] );
		unset( $_SESSION['twt_oauth_token_secret'] );
	}
	
	function getUserData( $criteria = array() )
	{
		$token = $criteria['token'];
		$token_secret = $criteria['token_secret'];
		
		if( $token == null || $token_secret == null )
		{
			$token = $this->twitter_token;
			$token_secret = $this->twitter_token_secret;
		}
		
		if( $token != null && $token_secret != null )
		{
			$connection = new TwitterOAuth( $this->getApiId(), $this->getApiSecret(), $token, $token_secret );
			
			$result = $connection->get( 'account/verify_credentials' );
		}
		
		return $result;
	}
	
	function getFollowers( $criteria = array() )
	{
		$user_id = $criteria['user_id'];
		$screen_name = $criteria['screen_name'];
		$cursor = $criteria['cursor'];
		
		if( $criteria['cursor'] == null )
		{
			$criteria['cursor'] = '-1';
		}
		
		$connection = new TwitterOAuth( $this->getApiId(), $this->getApiSecret(), $this->twitter_token, $this->twitter_token_secret );
		
		$users = $connection->get( 'followers/ids', $criteria );

		return $users;
	}
	
	function sendDirectMessageToAllUserFollowers( $criteria = array() )
	{
		$token = $criteria['token'];
		$token_secret = $criteria['token_secret'];
		
		if( $token == null || $token_secret == null )
		{
			$token = $this->twitter_token;
			$token_secret = $this->twitter_token_secret;
		}
		
		if( $token != null && $token_secret != null )
		{
			$connection = new TwitterOAuth( $this->getApiId(), $this->getApiSecret(), $token, $token_secret );
			
			$connection->get( 'account/verify_credentials' );
			
			$result = $connection->post( 'direct_messages/new', $criteria );
		}
		
		return $result;
	}
	
	function autoAddFollowerToAccount( $criteria = array() )
	{
		$token = $criteria['token'];
		$token_secret = $criteria['token_secret'];
		
		if( $token == null || $token_secret == null )
		{
			$token = $this->twitter_token;
			$token_secret = $this->twitter_token_secret;
		}
		
		if( $token != null && $token_secret != null )
		{
			$connection = new TwitterOAuth( $this->getApiId(), $this->getApiSecret(), $token, $token_secret );
			
			$connection->get( 'account/verify_credentials' );
			
			$result = $connection->post( 'friendships/create', $criteria );
		}
		
		return $result;
	}
	
	function updateTwitterStatus( $criteria = array() )
	{
		$status = $criteria['status'];
		$token = $criteria['token'];
		$token_secret = $criteria['token_secret'];
		
		if( $token == null || $token_secret == null )
		{
			$token = $this->twitter_token;
			$token_secret = $this->twitter_token_secret;
		}
		
		if( strlen( $status ) > 140 )
		{
			$status = substr( $status, 0, 140 );
		}
		
		if( $token != null && $token_secret != null )
		{
			$connection = new TwitterOAuth( $this->getApiId(), $this->getApiSecret(), $token, $token_secret );
			
			$connection->get( 'account/verify_credentials' );
			
			$result = $connection->post( 'statuses/update', $criteria );
		}
		
		return $result;
	}
}