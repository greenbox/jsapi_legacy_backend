<?php
if( ! extension_loaded('oauth') )
{
	require_once('OAuth.php');
}

class TwitterOAuth
{
	public $http_code;
	public $last_api_call;
	public $host = 'https://api.twitter.com/1/';
	public $timeout = 30;
	public $connecttimeout = 30; 
	public $ssl_verifypeer = false;
	public $format = 'json';
	public $decode_json = true;

	function accessTokenURL()
	{
		return 'https://twitter.com/oauth/access_token';
	}
	
	function authenticateURL()
	{
		return 'https://twitter.com/oauth/authenticate';
	}
	
	function authorizeURL()
	{ 
		return 'https://twitter.com/oauth/authorize';
	}
  
	function requestTokenURL()
	{ 
		return 'https://twitter.com/oauth/request_token'; 
	}

	function __construct( $consumer_key, $consumer_secret, $oauth_token = null, $oauth_token_secret = null )
	{
		$this->sha1_method = new OAuthSignatureMethod_HMAC_SHA1();
		$this->consumer = new OAuthConsumer( $consumer_key, $consumer_secret );
		
		if( ! empty( $oauth_token ) && ! empty( $oauth_token_secret ) )
		{
			$this->token = new OAuthConsumer( $oauth_token, $oauth_token_secret );
		}
		else
		{
			$this->token = null;
		}
	}

	function getRequestToken( $oauth_callback = null )
	{
		$parameters = array();
		
		if( ! empty( $oauth_callback ) )
		{
			$parameters['oauth_callback'] = $oauth_callback;
		}
		
		$request = $this->oAuthRequest( $this->requestTokenURL(), 'GET', $parameters );
		$token = OAuthUtil::parse_parameters( $request );
		
		$this->token = new OAuthConsumer( $token['oauth_token'], $token['oauth_token_secret'] );
		
		return $token;
	}

	function getAuthorizeURL( $token, $sign_in_with_twitter = true )
	{
		if( is_array( $token ) )
		{
			$token = $token['oauth_token'];
		}
    
		if( empty( $sign_in_with_twitter ) )
		{
			return $this->authorizeURL() . "?oauth_token={$token}";
		}
		else
		{
			return $this->authenticateURL() . "?oauth_token={$token}";
		}
	}

	function getAccessToken( $oauth_verifier = false )
	{
		$parameters = array();
		
		if( ! empty( $oauth_verifier ) )
		{
			$parameters['oauth_verifier'] = $oauth_verifier;
		}
		
		$request = $this->oAuthRequest( $this->accessTokenURL(), 'GET', $parameters );
		$token = OAuthUtil::parse_parameters( $request );
		
		$this->token = new OAuthConsumer( $token['oauth_token'], $token['oauth_token_secret'] );
		
		return $token;
	}
	
	function lastStatusCode()
	{ 
		return $this->http_status;
	}
	
	function lastAPICall()
	{
		return $this->last_api_call;
	}

	function get( $url, $parameters = array() )
	{
		$response = $this->oAuthRequest( $url, 'GET', $parameters );
		
		if( $this->format === 'json' && $this->decode_json )
		{
			return json_decode( $response );
		}
		
		return $response;
	}
	
	function post( $url, $parameters = array() )
	{
		$response = $this->oAuthRequest( $url, 'POST', $parameters );
   
		if( $this->format === 'json' && $this->decode_json )
		{
			return json_decode( $response );
		}
		
		return $response;
	}

	function delete( $url, $parameters = array() )
	{
		$response = $this->oAuthRequest( $url, 'DELETE', $parameters );
		
		if( $this->format === 'json' && $this->decode_json )
		{
			return json_decode( $response );
		}
		
		return $response;
	}

	function oAuthRequest( $url, $method, $parameters )
	{
		if( strrpos( $url, 'https://' ) !== 0 && strrpos( $url, 'http://' ) !== 0 )
		{
			$url = "{$this->host}{$url}.{$this->format}";
		}
		
		$request = OAuthRequest::from_consumer_and_token( $this->consumer, $this->token, $method, $url, $parameters );
		$request->sign_request( $this->sha1_method, $this->consumer, $this->token );
   
		switch( $method )
		{
			case 'GET':
			return $this->http( $request->to_url(), 'GET' );
			default:
			return $this->http( $request->get_normalized_http_url(), $method, $request->to_postdata() );
		}
	}
	
	function http( $url, $method, $postfields = NULL )
	{
		$ci = curl_init();

		curl_setopt( $ci, CURLOPT_CONNECTTIMEOUT, $this->connecttimeout );
		curl_setopt( $ci, CURLOPT_TIMEOUT, $this->timeout );
		curl_setopt( $ci, CURLOPT_RETURNTRANSFER, true );
		curl_setopt( $ci, CURLOPT_HTTPHEADER, array('Expect:') );
		curl_setopt( $ci, CURLOPT_SSL_VERIFYPEER, $this->ssl_verifypeer );

		switch( $method )
		{
			case 'POST':
			curl_setopt( $ci, CURLOPT_POST, true );
			if( ! empty( $postfields ) )
			{
				curl_setopt( $ci, CURLOPT_POSTFIELDS, $postfields );
			}
			break;
			case 'DELETE':
			curl_setopt( $ci, CURLOPT_CUSTOMREQUEST, 'DELETE' );
			if( ! empty( $postfields ) )
			{
				$url = "{$url}?{$postfields}";
			}
		}

		curl_setopt( $ci, CURLOPT_URL, $url );
		
		$response = curl_exec( $ci );
		
		$this->http_code = curl_getinfo( $ci, CURLINFO_HTTP_CODE );
		$this->last_api_call = $url;
		
		curl_close ( $ci );
		
		return $response;
	}
}
