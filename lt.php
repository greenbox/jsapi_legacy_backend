<?php
// /http://desktop/taskTrace/2013/07.2013/mp/lt.php?uri=/apps/:controller/loader.js
//lt.php?uri=/console_app
// /http://head/hg/jsapi/lt.php?uri=/apps/:controller/loader.js

//clear Version

require_once 'l.php';


function autoload_controller($callback) 
{ 
    require_once 'config.php';

    global $r ,$db;

    $r = S_PATH;
    $db = new edb(DB_HOST,DB_USER,DB_PASSWORD,DB_NAME);

    if ($callback == 'jsControll'){

        //CallBack option dir

        return 1;
    }
}


dispatch('/apps/:website/:callback/loader.js', 'jsControll');
dispatch('/xten/:website/:callback/:xkey/loader.js', 'jsControll');

function jsControll($website , $callback , $xkey = false){
    //var_dump($controller);
    global $db;
    $hasTag = md5(date('dmYhis'));
    $return = array('error' => 0 , 'code' => $hasTag);
    if ($xkey){
        $_SESSION[$hasTag] = array('time' => 'h:i:s' , 'site_open' => $website , 'subkey' => trim($xkey));  
    }else{
        $_SESSION[$hasTag] = array('time' => 'h:i:s' , 'site' => $website); 
    }   
    return render('cb.php', null, array('callback' => $callback , 'return' => json_encode($return) ));
}

//Console App Listing []
dispatch('/console_app/:website/:app_name', 'consAppLoader');

function consAppLoader($website , $app_name){
    global $r , $db;
    $appPath = 'js/conap/'.$app_name;
    if (file_exists($appPath)){
        die(file_get_contents($appPath));
    }else{
        $appName = str_replace('.app.js', '', $app_name);

        if (strpos($appName, '--') > 0){
            $appNameSplitted = explode("--", $appName);
            $appName = $appNameSplitted[0];
            $appType = $appNameSplitted[1];
            if (isset($appNameSplitted[2]) && trim($appNameSplitted[2]) != ''){
                $autorun = true;
            }
        }


        $result = $db->line("select * from `conap` where name LIKE '".$appName."' limit 1");
        //Build From DB Info if possible
        $cnt = false;
        if (isset($result['content'])){
            $cnt = str_replace('[PATH]' , $r , trim(base64_decode($result['content'])));
        }       

        return render('lst', null, array('appname' => $appName , 'content' => $cnt , 'type' => isset($appType) ? $appType : 'CNSApp' , 'autorun' => isset($autorun) ? $autorun : false));
    }

}



//Console App Listing []
dispatch('/console_app_m/:app_key' , 'multiAppLoad');

function multiAppLoad($app_key){
    global $r , $db;
    try {
        $appList = json_decode(base64_decode($app_key));
        if (is_array($appList) && count($appList) > 0){
            $result = $db->q("select * from `conap` where name IN ('".implode(" , ", $appList)."')");
            //Build From DB Info if possible
            if (count($result) > 0){
                $allApps = array();
                foreach ($result as $key => $value) {
                    $allApps[$value['name']] = str_replace('[PATH]' , $r , trim(base64_decode($value['content'])));
                }
                return render('lif', null, array('content' => $allApps));
            }
        }
    } catch (Exception $e) {
        die(json_encode($e->getMessage()));
    }       

    
}


dispatch('/phantom_app/:website/:app_name', 'phantomAppLoad');

function phantomAppLoad($website , $app_name){
    global $r , $db;
    $appPath = 'js/conap/'.$app_name;
    if (file_exists($appPath)){
        die(file_get_contents($appPath));
    }else{
        $appName = str_replace('.app.js', '', $app_name);
        $result = $db->line("select * from `conap` where name LIKE '".$appName."' limit 1");
        //Build From DB Info if possible
        $cnt = false;
        if (isset($result['content'])){
            $cnt = str_replace('[PATH]' , $r , trim(base64_decode($result['content'])));
        }       

        return render('phantlt', null, array('appname' => $appName , 'content' => $cnt , 'type' => isset($appType) ? $appType : 'CNSApp' , 'autorun' => isset($autorun) ? $autorun : false));
    }

}


dispatch('/blank_app/:website/:app_name', 'blankAppLoad');

function blankAppLoad($website , $app_name){
    global $r , $db;
    $appPath = 'js/conap/'.$app_name;
    if (file_exists($appPath)){
        die(file_get_contents($appPath));
    }else{
        $appName = str_replace('.app.js', '', $app_name);
        $result = $db->line("select * from `conap` where name LIKE '".$appName."' limit 1");
        //Build From DB Info if possible
        $cnt = false;
        if (isset($result['content'])){
            $cnt = str_replace('[PATH]' , $r , trim(base64_decode($result['content'])));
        }       

        return render('fsblank', null, array('path' => $r , 'full' => $cnt ));
    }

}




dispatch('/console/save/:website/:app_name/:callback', 'consoleAppSave');

function consoleAppSave($website , $app_name , $callback){
    ## Mega Seccurity issue [CONFUSED]text[/CONFUSED]
    global $r , $db;
    if (isset($app_name) && isset($_REQUEST['content'])){
        $result = $db->line("select * from `conap` where name LIKE '".$appName."' limit 1");
        //Build From DB Info if possible
        $cnt = false;
        if (isset($result['content'])){
            $q = "UPDATE conap SET content = '".trim($_REQUEST['content'])."' WHERE  name LIKE '".$appName."' limit 1";
        }else{
            $q = "INSERT INTO `conap` (`name`, `content`) VALUES ('".$app_name."', '".base64_encode(trim($_REQUEST['content']))."');";
        }

        return render('cb.php', null, array('callback' => $callback , 'return' => json_encode($db->q($q)))); 
    }else{
        die('Request missing');
    }
}


dispatch('/apps/:website/:callback/init.js', 'jsinit');
dispatch('/apps/:website/:callback/:hoderName/init.js', 'jsinit');

function jsinit($website , $callback , $hoderName = false){
    global $r;
    $params = array('path' => $r);
    $rederName = 'nit';
    if ($hoderName && trim($hoderName) != ''){
        $params['holder'] = $hoderName;
        $rederName = 'cNit';
    }
    return render($rederName, null, $params);
}

dispatch('/apps/:code/core.js', 'jscore');

function jscore($code){
    global $r , $db;

    $return = array('sucess' => 0);
    $cnt = '';

    $render = 'inval';

    if (isset($_SESSION[$code]) && count($_SESSION[$code]) > 0){
        if (isset($_SESSION[$code]['site'])){
            $result = $db->line("select * from `domains` where name LIKE '".$_SESSION[$code]['site']."' limit 1");
            $return['sucess'] = 1; 
        }else if (isset($_SESSION[$code]['subkey'])){
            $result = $db->line("select * from `apps` where name LIKE '".$_SESSION[$code]['subkey']."' limit 1");
            $return['sucess'] = 1; 
        }
        if (isset($result['key'])){
            //Special Request || Look what apps and users are allowed
            if (isset($_SESSION[$code]['subkey']) && $_SESSION[$code]['subkey'] == 'phantom'){
                $render = 'fsphant';    
            }else if (isset($_SESSION[$code]['subkey'])){
                //select from conapp list
                $result = $db->line("select * from `conap` where name LIKE '".$_SESSION[$code]['subkey']."' limit 1");
                //Build From DB Info if possible
                $cnt = false;
                if (isset($result['content'])){
                    $render = 'fsblank';
                    $cnt = str_replace('[PATH]' , $r , trim(base64_decode($result['content'])));
                }else{
                    $render = $_SESSION[$code]['subkey'];
                }   
            }else{
                $render = 'index.php';
            }
        }else{
            $render = 'fs'; 
        }
    }   
    return render($render, null, array('callback' => 'console.log' , 'path' => $r , 'full' => $cnt , 'return' => json_encode($return)));
}

dispatch('/reg/:website/:callback/register.js', 'registerSite');

function registerSite($website , $callback){
    global $r , $db;
    //require registration of UserName , but Now add as registred add
    if (isset($website) && trim($website) != ''){
        $result = $db->q('INSERT INTO domains (`key` , `name`) VALUES (1 , "'.$website.'")');
    }else{
        $result = false;
    }

    return render('cb.php', null, array('callback' => $callback , 'return' => json_encode($result))); 

}

//app save
dispatch('/app-editor', 'addEditor');
function addEditor(){
    global $r , $db;
    $appName = 'test';
    if(isset($_REQUEST['content'])){
        $content = explode(":::" , $_REQUEST['content']);
        if (!isset($content[1])){
            die('No content');
        }else{
            $appName = trim($content[0]);
        }
        
        $result = $db->line("select * from `conap` where name LIKE '".$content[0]."' limit 1");
        //Build From DB Info if possible
        $cnt = false;
        if (isset($result['content'])){
            //backup old
            $q = "UPDATE conap SET name = 'tmp_app_bckup_".$content[0]."_".date('dMYHis')."' WHERE  name LIKE '".$content[0]."' limit 1";
            $db->q($q);
        }

        //insert New
        $q = "INSERT INTO `conap` (`name`, `content`) VALUES ('".$content[0]."', '".base64_encode(trim($content[1]))."');";

        var_dump($content);
        die($db->q($q)); 
    }else if (isset($_REQUEST['content_request'])){
        $content = explode(":::" , $_REQUEST['content_request']);
        $appName = trim($content[0]);
        
        $result = $db->line("select * from `conap` where name LIKE '".$content[0]."' limit 1");
        //Build From DB Info if possible
        $cnt = false;
        if (isset($result['content'])){
            die($result['name'].':::'.base64_decode(htmlentities($result['content'])));
        }
    }
}


dispatch('/app-by-date/:date', 'dateExtract');
function dateExtract($date = false){
    global $r , $db;
    try {
        if (!$date){
            $date = date('d-m-Y');
        }
        $endTime = strtotime($date);
        $startTime = $endTime - 86400;
        //1379808000
        //86400000
        //86400
        $selSq = "select * from `conap` where name BETWEEN ".$startTime."000 AND ".$endTime."000 ";
        $result = $db->q($selSq);
        //Build From DB Info if possible
        if (count($result) > 0){
            die(json_encode($result));
        }
    } catch (Exception $e) {
        die(json_encode($e->getMessage()));
    }   
}


run();