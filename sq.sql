CREATE TABLE `operations` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`location_key` VARCHAR(50) NULL DEFAULT NULL,
	`location_type` ENUM('domain','link') NULL DEFAULT NULL,
	`source` VARCHAR(50) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `location_key` (`location_key`),
	INDEX `location_type` (`location_type`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM;


CREATE TABLE `domains` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`key` VARCHAR(10) NULL DEFAULT NULL,
	`name` VARCHAR(100) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `key` (`key`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM;

CREATE TABLE `conap` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`name` VARCHAR(50) NOT NULL DEFAULT '0',
	`content` VARCHAR(5000) NOT NULL DEFAULT '0',
	PRIMARY KEY (`id`),
	INDEX `name` (`name`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM;

CREATE TABLE `apps` (
	`id` INT(10) NOT NULL AUTO_INCREMENT,
	`key` VARCHAR(10) NULL DEFAULT NULL,
	`name` VARCHAR(500) NULL DEFAULT NULL,
	PRIMARY KEY (`id`),
	INDEX `key` (`key`)
)
COLLATE='utf8_general_ci'
ENGINE=MyISAM;

ALTER TABLE `conap`
	ADD COLUMN `created` TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP AFTER `id`,
	ADD INDEX `created` (`created`);

ALTER TABLE `conap`
	ADD COLUMN `owner` INT(10) NULL DEFAULT '0' AFTER `content`,
	ADD INDEX `owner` (`owner`);